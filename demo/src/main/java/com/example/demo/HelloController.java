package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/hello/{name}")
	public String helloUser(@PathVariable("name") String name) {
		return "Hello "+name;
	}
	
	@GetMapping("/hi/{name}")
	public String hiUser(@PathVariable("name") String name) {
		return "Hi "+name;
	}
	
	@GetMapping
	public String welcomeUser() {
		return "Welcome to Spring";
	}
	
}
